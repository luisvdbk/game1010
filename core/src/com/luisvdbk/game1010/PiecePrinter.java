package com.luisvdbk.game1010;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
public class PiecePrinter {

	private PieceGenerator pieceGenerator;
	private GraphicBoxShape setOfTextures;
	private float boxWidth;
	private float boxHeight;
	private float smallBoxWidth;
	private float smallBoxHeight;
	private float startPointY;
	private float printerWidth;
	private float printerHeight;
	private float printerMargin = 35;
	private Array<Piece> currentPieces;
	private float pieceSquaresWidth;//This is where the pieces will be centered.
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Sound generatePiecesSound;

	public PiecePrinter(PieceGenerator pieceGenerator, GraphicBoxShape setOfTextures, float[] boxDimensions, float startPointY, float printerW,  float printerH, OrthographicCamera camera, SpriteBatch batch, Sound generatePiecesSound) {
		this.pieceGenerator = pieceGenerator;
		this.setOfTextures = setOfTextures;
		this.boxWidth = boxDimensions[0];
		this.boxHeight = boxDimensions[1];
		this.smallBoxWidth = (boxWidth * 80) / 100;//Will be 30 smaller than the boxes in the board
		this.smallBoxHeight = (boxHeight * 80) / 100;
		this.startPointY = startPointY;
		this.printerWidth = printerW - (2 * printerMargin);
		this.printerHeight = printerH;
		currentPieces = new Array<Piece>();
		this.pieceSquaresWidth = printerWidth / pieceGenerator.getIterator();
		this.camera = camera;
		this.batch = batch;
		this.generatePiecesSound = generatePiecesSound;

	}

	public void generatePieces() {
		//DEBUG
		//Gdx.app.setLogLevel(Application.LOG_DEBUG);
		
		currentPieces = pieceGenerator.getRandomPieces();
		int i = 0;
		for (Piece currentPiece : currentPieces) {

			float pieceWidth = currentPiece.getWidth() * smallBoxWidth;
			float pieceHeight = currentPiece.getHeight() * smallBoxHeight;
			Color pieceColor = currentPiece.getColor();//its color
			
			Texture pieceTexture = setOfTextures.getTextureByColor(pieceColor);

			Sprite pivot =  new Sprite(pieceTexture);//its pivot
			pivot.setSize(smallBoxWidth, smallBoxHeight);
			float pivotPosX = pieceMiddlePosX((pieceSquaresWidth * i) + printerMargin, pieceWidth);
			float pivotPosY = pieceMiddlePosY(pieceHeight);
			pivot.setPosition(pivotPosX, pivotPosY);
			currentPiece.setInitialPos(pivotPosX, pivotPosY);

			Position[] pieceCoords = currentPiece.getCoords();

			Array<Sprite> pieceSprites = new Array<Sprite>();//This will contain the sprites of the piece,always starting with the pivot
			pieceSprites.add(pivot);
			Sprite currentSprite = pivot;
			for (Position pieceBox : pieceCoords) {
				Sprite newSprite = new Sprite(pieceTexture);
				float[] newSpritePosition = pieceBox.getBoxCoords(currentSprite.getX(), currentSprite.getY(), smallBoxWidth, smallBoxHeight);
				newSprite.setSize(smallBoxWidth, smallBoxHeight);
				newSprite.setPosition(newSpritePosition[0], newSpritePosition[1]);
				pieceSprites.add(newSprite);
				currentSprite = newSprite;
			}

			currentPiece.setSprites(pieceSprites);
			i++;
		}
		generatePiecesSound.play();
		renderPieces();
	}

	public void renderPieces(){
		
		if (currentPieces.size == 0) {
			generatePieces();
		} else {
			camera.update();
		    batch.setProjectionMatrix(camera.combined);
			batch.begin();
			for (Piece piece : currentPieces) {
				Array<Sprite> pieceSprites = piece.getSprites();
				for (Sprite sprite : pieceSprites) {
					sprite.draw(batch);
				}
			}
			batch.end();
		}
	}


	public void removePieceFromList(Piece piece){
		currentPieces.removeValue(piece, false);
	}

	public float pieceMiddlePosX(float initialPos, float pieceWidth) {
		float referencePos = (pieceSquaresWidth/2) - (pieceWidth/2);
		return initialPos + referencePos;
	}

	public float pieceMiddlePosY(float pieceHeight) {
		float referencePos = (printerHeight/2) - (pieceHeight/2);
		return startPointY + referencePos;
	}

	public Piece touchedPiece(float x, float y) {
		Vector3 coords = new Vector3(x, y, 0);
		camera.update();
		camera.unproject(coords);
		for (Piece piece : currentPieces) {
			Array<Sprite> pieceSprites = piece.getSprites();
			for (Sprite sprite : pieceSprites) {
				Rectangle spriteRectangle = sprite.getBoundingRectangle();
				if (spriteRectangle.contains(coords.x, coords.y)) {
					return piece;
				}
			}
		}
		return null;
	}

	public void pieceUpOnTouch(Piece piece, float x, float y) {
		Vector3 coords = new Vector3(x, y, 0);
		camera.update();
		camera.unproject(coords);
		float newX = coords.x - ((piece.getWidth() * boxWidth)/2);
		float newY = coords.y - (piece.getHeight() * boxHeight) - boxHeight;
		piece.changePiecePosition(newX, newY);
	}

	

	public void changeToRegularSize(Piece piece) {
		piece.changePieceSize(boxWidth, boxHeight);
	}

	public void changeToSmallerSize(Piece piece) {
		piece.changePieceSize(smallBoxWidth, smallBoxHeight);
	}

	public Array<Piece> getCurrentPieces(){
		return currentPieces;
	}

	
}