package com.luisvdbk.game1010;
import com.badlogic.gdx.utils.Array;

public class GameEndChecker {

	private Board board;
	private Box[][] boardBoxes;
	private int boardRows;
	private int boardCols;
	private PiecePrinter piecePrinter;

	public GameEndChecker(Board board, PiecePrinter piecePrinter){
		this.board = board;
		this.boardBoxes = board.getBoxes();
		int[] dimensions = board.getDimensions();
		this.boardRows = dimensions[0];
		this.boardCols = dimensions[1];
		this.piecePrinter = piecePrinter;
	}

	public boolean isGameEnd() {
		Array<Piece> currentPieces = piecePrinter.getCurrentPieces();
		for (Piece piece : currentPieces) {
			for (int i = 0; i < boardRows; i++) {
				for (int j = 0; j < boardCols; j++) {
					Boolean canPlacePiece = piece.canPlacePiece(boardBoxes[i][j]);
					if (canPlacePiece) {
						return false;
					}
				}
			}
		}
		return true;
	}
}