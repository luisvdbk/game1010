package com.luisvdbk.game1010;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

public abstract class BoxCollectionHandler {
	protected BoxCollection[] collections;
	protected int numberOfCollections;

	public abstract BoxCollection[] setCollections();
	public abstract int getNumberOfCollections();

	public int checkCollections() {
		Array<BoxCollection> fullCollections = new Array<BoxCollection>();
		for (int i = 0; i < numberOfCollections; i++) {
			BoxCollection actualCollection = collections[i];
			if (actualCollection.isFull()) {
				fullCollections.add(actualCollection);
			}
		}
		if (fullCollections.size > 0) {
			return clearCollections(fullCollections);
		} else {
			return 0;
		}
	}

	public int clearCollections(Array<BoxCollection> collections){
		int acumPoints = 0;
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		for (BoxCollection collection : collections) {
			Gdx.app.debug("test", "paso");
			acumPoints += collection.clearBoxes();
		}
		return acumPoints;

	}
}

