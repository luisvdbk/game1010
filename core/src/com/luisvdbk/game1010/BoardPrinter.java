package com.luisvdbk.game1010;
public interface BoardPrinter {

	public void printBoard();
	public void createBoardSprites();
	public float[] getBoxDimensions();
	public float getBoardHeight();
	public boolean checkPieceOnBoard(Piece piece);
	
}