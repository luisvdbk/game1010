package com.luisvdbk.game1010;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
public class Piece {
	private PieceShape shape;
	private Position[] coordinates;
	private Array<Box> filledBoxes;/*Boxes that the piece is filling*/
	private int state;/*0 if the pice hasnt been played, and 1 if it has been played*/
	private int points;/*Points that the pieces gives to the playes when places it on the board*/
	private Color color;
	private int width;//The width and height are measured in number of boxes.
	private int height;
	private Array<Sprite> pieceSprites;
	private float initialPosX;
	private float initialPosY;


	public Piece(PieceShape shape){
		this.shape = shape;
		this.state = 0;
		coordinates = shape.getCoordinates();
		this.color = shape.getColor();
		int[] dimensions = shape.getDimensions();
		this.width = dimensions[0];
		this.height = dimensions[1];
		filledBoxes = new Array<Box>();
		pieceSprites = new Array<Sprite>();//Pivot will alsways be the first element of this array.
		this.points = coordinates.length + 1;

	}

	public PieceShape getShape(){
		return shape;
	}

	public int getState() {
		return state;
	}

	public Position[] getCoords() {
		return coordinates;
	}

	public Color getColor() {
		return color;
	}

	public int getPoints(){
		return points;
	}

	public int getWidth(){
		return width;
	}

	public int getHeight(){
		return height;
	}

	public float getInitX() {
		return initialPosX;
	}

	public float getInitY() {
		return initialPosY;
	}

	public Array<Sprite> getSprites() {
		return pieceSprites;
	}

	public void setSprites(Array<Sprite> sprites) {
		pieceSprites = sprites;
	}

	public void setInitialPos(float x, float y) {
		this.initialPosX = x;
		this.initialPosY = y;
	}

	/*Try to put a piece, according to its coordinates, in certain boxes*/
	public boolean placePiece(Box pivot) {
		/*You cant place a piece if the pivot is already filled or if the piece is already placed*/
		if (pivot.getState() == null && state == 0) {
			Box currentBox = pivot;
			Array<Box> boxesToFill = new Array<Box>();
			boxesToFill.add(currentBox);
			for (int i = 0; i < coordinates.length; i++) {
				Box testBox = currentBox.hasAdjacency(coordinates[i]);
				if (testBox != null && testBox.getState() == null) {
					currentBox = testBox;
					boxesToFill.add(currentBox);
				} else {
					return false;
				}
			}

			for (Box box : boxesToFill) {
				box.setState(this);
				box.setColor(color);
			}
			filledBoxes = boxesToFill;
			return true;

		} else {
			return false;
		}
	}

	public boolean canPlacePiece(Box pivot) {
		if (pivot.getState() == null && state == 0) {
			Box currentBox = pivot;
			for (int i = 0; i < coordinates.length; i++) {
				Box testBox = currentBox.hasAdjacency(coordinates[i]);
				if (testBox != null && testBox.getState() == null) {
					currentBox = testBox;
				} else {
					return false;
				}
			}

			return true;

		} else {
			return false;
		}
	}

	public void changePiecePosition(float pivotX, float pivotY) {
		float posX = pivotX;
		float posY = pivotY;
		Sprite pivot = pieceSprites.first();
		pivot.setPosition(posX, posY);
		for (int i = 0; i < coordinates.length; i++) {
			Sprite currentSprite = pieceSprites.get(i + 1);
			float[] boxCoords = coordinates[i].getBoxCoords(posX, posY, currentSprite.getWidth(), currentSprite.getHeight());
			posX = boxCoords[0];
			posY = boxCoords[1];
			currentSprite.setPosition(posX, posY);
		}
	}

	public void changePieceSize(float boxW, float boxH){
		//Array<Sprite> pieceSprites = piece.getSprites();
		Sprite pivot = pieceSprites.first();
		pivot.setSize(boxW, boxH);
		float posX = pivot.getX();
		float posY = pivot.getY();
		//Position pieceCoords = piece.getCoords();
		for (int i = 0; i < coordinates.length; i++) {
			Sprite currentSprite = pieceSprites.get(i + 1);
			currentSprite.setSize(boxW, boxH);
			float[] boxCoords = coordinates[i].getBoxCoords(posX, posY, currentSprite.getWidth(), currentSprite.getHeight());
			posX = boxCoords[0];
			posY = boxCoords[1];
			currentSprite.setPosition(posX, posY);
		}
	}

	public void returnToInitPos() {
		changePiecePosition(initialPosX, initialPosY);
	}

	public Vector2 getPivotCenter(){
		Sprite pivot = pieceSprites.first();
		Vector2 center = new Vector2();
		return pivot.getBoundingRectangle().getCenter(center);
	}
}