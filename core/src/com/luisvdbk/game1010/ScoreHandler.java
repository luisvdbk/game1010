package com.luisvdbk.game1010;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ScoreHandler {

	private int actualScore = 0;
	private int highScore;
	private float width;
	private String scoreMessage;
	private float scoreMessagePosX;
	private BitmapFont myFont;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Preferences prefs;

	public ScoreHandler(float zoneWidth, OrthographicCamera camera, SpriteBatch batch){
		this.highScore = getPrefs().getInteger("highScore", 0); 
		this.scoreMessage = "Score : "+String.valueOf(actualScore)+"   HighScore : "+String.valueOf(highScore);
		this.scoreMessagePosX = (zoneWidth/2) - 250; //it will be 70% of the world width
		this.myFont = new BitmapFont(Gdx.files.internal("verdana39.fnt"),Gdx.files.internal("verdana39.png"),true);
		myFont.setColor(0, 0, 0, 1);
		this.camera = camera;
		this.batch = batch;
	}

	public void updatePoints(int points) {
		actualScore += points;
		if (actualScore > getPrefs().getInteger("highScore", 0)) {
			getPrefs().putInteger("highScore", actualScore);
			getPrefs().flush();
			highScore = getPrefs().getInteger("highScore");
		}
		scoreMessage = "Score : "+String.valueOf(actualScore)+"   HighScore : "+String.valueOf(highScore);
	}

	public void renderPoints() {
		camera.update();
        batch.setProjectionMatrix(camera.combined);
		batch.begin(); 
		myFont.draw(batch, scoreMessage, scoreMessagePosX, 100);
		batch.end();
	}

	public Preferences getPrefs() {
      if(prefs==null){
         prefs = Gdx.app.getPreferences("MY_PREFS");
      }
      return prefs;
   }

   public int getActualScore() {
   		return actualScore;
   }
}