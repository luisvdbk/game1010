package com.luisvdbk.game1010;

public enum RegularPieceShape implements PieceShape{

	/*coordinates are defined from the pivot. So, if the piece fills 5 spaces, there will be 4 coordinates, beacause one is already set by the pivot*/
	L1_LONG(new Position[] {Position.DOWN, Position.DOWN, Position.RIGHT, Position.RIGHT}, Color.BLUE, 3, 3),
	L2_LONG(new Position[] {Position.DOWN, Position.DOWN, Position.LEFT, Position.LEFT}, Color.BLUE, -2, 3),
	L3_LONG(new Position[] {Position.RIGHT, Position.RIGHT, Position.DOWN, Position.DOWN}, Color.BLUE, 3, 3),
	L4_LONG(new Position[] {Position.LEFT, Position.LEFT, Position.DOWN, Position.DOWN}, Color.BLUE, -2, 3),
	L1_SHORT(new Position[] {Position.DOWN, Position.RIGHT}, Color.RED, 2, 2),
	L2_SHORT(new Position[] {Position.DOWN, Position.LEFT}, Color.RED, -1, 2),
	L3_SHORT(new Position[] {Position.RIGHT, Position.DOWN}, Color.RED, 2, 2),
	L4_SHORT(new Position[] {Position.LEFT, Position.DOWN}, Color.RED, -1, 2),
	DOUBLE_LINE_VERTICAL(new Position[] {Position.DOWN}, Color.GREEN, 1, 2),
	DOUBLE_LINE_HORIZONTAL(new Position[] {Position.RIGHT}, Color.GREEN, 2 , 1),
	TRIPLE_LINE_VERTICAL(new Position[] {Position.DOWN, Position.DOWN}, Color.GREEN, 1 , 3),
	TRIPLE_LINE_HORIZONTAL(new Position[] {Position.RIGHT, Position.RIGHT}, Color.GREEN, 3 , 1),
	QUADRUPLE_LINE_VERTICAL(new Position[] {Position.DOWN, Position.DOWN, Position.DOWN}, Color.GREEN, 1 , 4),
	QUADRUPLE_LINE_HORIZONTAL(new Position[] {Position.RIGHT, Position.RIGHT, Position.RIGHT}, Color.GREEN, 4, 1),
	QUINTUPLE_LINE_VERTICAL(new Position[] {Position.DOWN, Position.DOWN, Position.DOWN, Position.DOWN}, Color.GREEN, 1 , 5),
	QUINTUPLE_LINE_HORIZONTAL(new Position[] {Position.RIGHT, Position.RIGHT, Position.RIGHT, Position.RIGHT}, Color.GREEN, 5, 1),
	SINGLE_SQUARE(new Position[] {}, Color.YELLOW, 1, 1),
	DOUBLE_SQUARE(new Position[] {Position.RIGHT, Position.DOWN, Position.LEFT}, Color.YELLOW, 2, 2),
	TRIPLE_SQUARE(new Position[] {Position.RIGHT, Position.RIGHT, Position.DOWN, Position.DOWN, Position.LEFT, Position.LEFT, Position.UP, Position.RIGHT}, Color.YELLOW, 3, 3);


	/*IMPORTANT: IT'S NECESSARY TO SET THE COORDINATES FROM THE HIGHER POINT OF THE SHAPE TO THE LOWER POINT OF THE SHAPE*/
	/*The L shapes that are contructed in left direction neeed a negative value in their width*/



	private Position[] coordinates;
	private Color color;
	private int width;
	private int height;


    private RegularPieceShape(Position[] coordinates, Color color, int width, int height) {
        this.coordinates = coordinates;
        this.color = color;
        this.width = width;
        this.height = height;
    }

 	public Position[] getCoordinates(){
    	return coordinates;
    }

    public Color getColor(){
    	return color;
    }

    public int[] getDimensions() {
    	int[] dimensions = new int[2];
    	dimensions[0] = width;
    	dimensions[1] = height;
    	return dimensions;
    }


   

}