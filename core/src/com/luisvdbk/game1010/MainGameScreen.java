package com.luisvdbk.game1010;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MainGameScreen extends ScreenAdapter {
	private final Game1010 game;
	private OrthographicCamera camera;
	private SpriteBatch batch;

	private Board board;
	private GraphicBoxShape setOfBoxTextures;
	private BoardPrinter boardPrinter;
	private PieceGenerator pieceGenerator;
	
	private final int worldWidth;
	private final int worldHeight;
	private final int scoreZoneHeight = 400;

	private float[] boxDimensions;

	private float boardHeight;
	
	private PiecePrinter piecePrinter;
	private float piecePrinterPosY;
	private float piecePrinterHeight;

	private BoxCollectionHandler collectionHandler;

	private ScoreHandler scoreHandler;
	private GameEndChecker gameEndChecker;

	private Sound placedPieceSound;
	private Sound generatePiecesSound;


	public MainGameScreen(final Game1010 game) {
		this.game = game;
		this.camera = game.getCamera();
		this.batch = game.getBatch();
		this.worldWidth = game.getWorldWidth();
		this.worldHeight = game.getWorldHeight();

		this.placedPieceSound = Gdx.audio.newSound(Gdx.files.internal("sound1.mp3"));
		this.generatePiecesSound = Gdx.audio.newSound(Gdx.files.internal("sound2.mp3"));

		board = new Board(new SquaredBoard(10, 10));
		setOfBoxTextures = new GraphicSquaredShape();
		boardPrinter = new SquaredBoardPrinter(board, worldWidth, scoreZoneHeight, setOfBoxTextures, camera, batch);

		boxDimensions = boardPrinter.getBoxDimensions();

		PieceGenerator<RegularPieceShape> pieceGenerator = new PieceGenerator<RegularPieceShape>(RegularPieceShape.class);

		boardHeight = boardPrinter.getBoardHeight();

		piecePrinterPosY = scoreZoneHeight + boardHeight + 30;
		piecePrinterHeight = worldHeight - piecePrinterPosY;

		piecePrinter = new PiecePrinter(pieceGenerator, setOfBoxTextures, boxDimensions, piecePrinterPosY, worldWidth, piecePrinterHeight, camera, batch, generatePiecesSound);

		collectionHandler = new RowsAndCols(board);

		boardPrinter.createBoardSprites();


		scoreHandler = new ScoreHandler(worldWidth, camera, batch);
		gameEndChecker = new GameEndChecker(board, piecePrinter);

		MyInputProcessor inputProcessor = new MyInputProcessor(game, this, boardPrinter, piecePrinter, collectionHandler, scoreHandler, gameEndChecker, placedPieceSound);
		Gdx.input.setInputProcessor(inputProcessor);

		Gdx.graphics.setContinuousRendering(false);
		Gdx.graphics.requestRendering();


	}


	@Override
	public void render (float delta) {
		Gdx.gl.glClearColor(255, 255, 255, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//DEBUG
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		boardPrinter.printBoard();
		piecePrinter.renderPieces();
		scoreHandler.renderPoints();
		/*
		Gdx.app.debug("WorldHeight", String.valueOf(worldHeight));
		Gdx.app.debug("BoardHeight", String.valueOf(boardHeight));
		Gdx.app.debug("piecePrinterPosY", String.valueOf(piecePrinterPosY));
		Gdx.app.debug("piecePrinterHeight", String.valueOf(piecePrinterHeight));
		*/	
	}

	@Override
	public void dispose(){
		setOfBoxTextures.disposeTextures();
		placedPieceSound.dispose();
		generatePiecesSound.dispose();
	}
}
