package com.luisvdbk.game1010;
public enum Position {
	UP {
		public float[] getBoxCoords(float actualx, float actualy, float boxWidth, float boxHeight) {
			float[] coords = new float[2];
			coords[0] = actualx;
			coords[1] = actualy - boxHeight;
			return coords;
		}
	},
	DOWN {
		public float[] getBoxCoords(float actualx, float actualy, float boxWidth, float boxHeight) {
			float[] coords = new float[2];
			coords[0] = actualx;
			coords[1] = actualy + boxHeight;
			return coords;
		}
	},
	LEFT {
		public float[] getBoxCoords(float actualx, float actualy, float boxWidth, float boxHeight) {
			float[] coords = new float[2];
			coords[0] = actualx - boxWidth;
			coords[1] = actualy;
			return coords;
		}
	},
	RIGHT {
		public float[] getBoxCoords(float actualx, float actualy, float boxWidth, float boxHeight) {
			float[] coords = new float[2];
			coords[0] = actualx + boxWidth;
			coords[1] = actualy;
			return coords;
		}
	};

	public abstract float[] getBoxCoords(float actualx, float actualy, float boxWidth, float boxHeight);
}