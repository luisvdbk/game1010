package com.luisvdbk.game1010;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
public class SquaredBoardPrinter implements BoardPrinter {

	private GraphicBoxShape setOfTextures;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Board board;
	private Box[][] boardBoxes;
	private int boardRows;
	private int boardCols;
	private float boxWidth;
	private float boxHeight;
	private float boardSideMargin = 75;
	private float boardTopMargin;

	public SquaredBoardPrinter(Board board, int virtualWidth, float scoreZoneHeight,GraphicBoxShape setOfTextures, OrthographicCamera camera, SpriteBatch batch) {
		this.board = board;
		this.boardBoxes = board.getBoxes();
		int[] dimensions = board.getDimensions();
		this.boardRows = dimensions[0];
		this.boardCols = dimensions[1];
		this.boxWidth = (virtualWidth - (boardSideMargin * 2))/boardCols;
		this.boxHeight = boxWidth;//Because we want square boxes
		this.boardTopMargin = scoreZoneHeight;
		this.setOfTextures = setOfTextures;
		this.camera = camera;
		this.batch = batch;
	}

	public void createBoardSprites(){
		float startingX = boardSideMargin;
		float startingY = boardTopMargin;
		for (int i = 0; i < boardRows; i++) {
			for (int j = 0; j < boardCols; j++) {
				Color boxColor = boardBoxes[i][j].getColor();
				Texture boxTexture = setOfTextures.getTextureByColor(boxColor);
				Sprite boxSprite = new Sprite(boxTexture);
				boxSprite.setSize(boxWidth, boxHeight);
				boxSprite.setPosition((j * boxWidth) + startingX, (i * boxHeight) + startingY);
				boardBoxes[i][j].setSprite(boxSprite);
			}
		}
	}

	public void printBoard() {
		camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
		for (int i = 0; i < boardRows; i++) {
			for (int j = 0; j < boardCols; j++) {
				Sprite boxSprite = boardBoxes[i][j].getSprite();
				Color boxColor = boardBoxes[i][j].getColor();
				Texture boxTexture = setOfTextures.getTextureByColor(boxColor);
				boxSprite.setTexture(boxTexture);
				boxSprite.draw(batch);
			}
		}
		batch.end();
	}

	public boolean checkPieceOnBoard(Piece piece) {
		Vector2 pivotCenter = piece.getPivotCenter();
		for (int i = 0; i < boardRows; i++) {
			for (int j = 0; j < boardCols; j++) {
				Sprite boxSprite = boardBoxes[i][j].getSprite();
				if (boxSprite.getBoundingRectangle().contains(pivotCenter)) {
					if (piece.placePiece(boardBoxes[i][j])) {
						return true;
					} else {
						return false;
					}
				} 
			}
		}
		return false;
	}

	public float[] getBoxDimensions() {
		float[] dimensions = new float[2];
		dimensions[0] = boxWidth;
		dimensions[1] = boxHeight;
		return dimensions;
	}

	public float getBoardHeight() {
		return boardRows * boxHeight;
	}
}