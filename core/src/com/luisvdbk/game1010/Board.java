package com.luisvdbk.game1010;
import com.luisvdbk.game1010.BoardShape;
public class Board {

	/*My boadr will be a 2d array of boxes, any representation (square, hezagon, pyramid) will be in a 2d array*/
	private Box[][] boxes;
	//private int numOfBoxes;
	private BoardShape boardShape;

	public Board(BoardShape boardShape) {
		this.boardShape = boardShape;
		this.boxes = boardShape.getBoxes();
	}

	
	public Box getBoxByPosition(int x, int y) {
		return boardShape.getBoxByPosition(x, y);
	}

	public Box getBoxById(int id) {
		return boardShape.getBoxById(id);
	}

	public Box[][] getBoxes() {
		return boxes;
	}


	public int[] getDimensions() {
		return boardShape.getDimensions();
	}
}