package com.luisvdbk.game1010;
import java.util.HashMap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.Gdx;
public class GraphicSquaredShape extends GraphicBoxShape {

	public GraphicSquaredShape(){
		colorOptions = new HashMap<Color, Texture>();
		colorOptions.put(Color.DEFAULT, new Texture(Gdx.files.internal("grey.png")));
		colorOptions.put(Color.BLUE, new Texture(Gdx.files.internal("blue.png")));
		colorOptions.put(Color.GREEN, new Texture(Gdx.files.internal("green.png")));
		colorOptions.put(Color.RED, new Texture(Gdx.files.internal("red.png")));
		colorOptions.put(Color.YELLOW, new Texture(Gdx.files.internal("yellow.png")));
	}
}