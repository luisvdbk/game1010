package com.luisvdbk.game1010;
import com.badlogic.gdx.Gdx;
public abstract class BoxCollection{

	protected Box[] boxes;
	protected int numOfBoxes;

	public BoxCollection(Box[] collectBoxes) {
		boxes = new Box[collectBoxes.length];
		boxes = collectBoxes;
		numOfBoxes = boxes.length;
	}

	public boolean isFull() {
		for (int i =0; i < numOfBoxes; i++) {
			if (boxes[i].getState() == null) {
				return false;
			}
		}

		return true;
	}

	public int clearBoxes(){
		int acumPoints = 0;
		for (int i=0; i < numOfBoxes; i++) {
			boxes[i].setState(null);
			boxes[i].setColor(Color.DEFAULT);
			acumPoints++;
		}
		return acumPoints;
	}

}