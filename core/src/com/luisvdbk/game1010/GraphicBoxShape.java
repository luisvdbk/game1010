package com.luisvdbk.game1010;
import java.util.HashMap;
import java.util.Map;
import com.badlogic.gdx.graphics.Texture;
public abstract class GraphicBoxShape {

	protected HashMap<Color, Texture> colorOptions;
	
	public Texture getTextureByColor(Color color){
		return colorOptions.get(color);
	}

	public void disposeTextures(){
		for (Texture texture : colorOptions.values()) {
			texture.dispose();
		}
	}
}