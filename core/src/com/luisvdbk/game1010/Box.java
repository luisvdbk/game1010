package com.luisvdbk.game1010;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.HashMap;
public class Box {

	private int id;
	private int posx;
	private int posy;
	private Piece state; //Because a box   can be filled by a piece. If empty set to null, else set to a piece object
	private HashMap<Position, int[]> adjCoords;
	private HashMap<Position, Box> adjacencies;	
	private TypeOfBox typeOfBox;
	private Sprite sprite;
	private Color color;
	
	public Box(int id, int posx, int posy, TypeOfBox typeOfBox) {
		this.id = id;
		this.posx= posx;
		this.posy = posy;
		this.state = null;
		this.typeOfBox = typeOfBox;
		adjCoords = new HashMap<Position, int[]>();
		adjCoords = typeOfBox.setAdjCoords();
		adjacencies = new HashMap<Position, Box>();
		this.color = Color.DEFAULT;
	}

	/*GETTERS*/
	
	public int getId(){
		return id;
	}
	

	public Piece getState(){
		return state;
	}

	public HashMap<Position, int[]> getAdjCoords() {
		return adjCoords;
	}
	
	public HashMap<Position, Box> getAdjacencies(){
		return adjacencies;
	}

	public Color getColor(){
		return color;
	}

	public Sprite getSprite() {
		return sprite;
	}

	/*SETTERS*/
	public void setState(Piece piece){
		this.state = piece;
	}

	public void setAdjacencies(HashMap<Position, Box> adj){
		this.adjacencies = adj;
	}

	/*check if an adyacency exists, returns adyacent box or null if adjacency doesnt exists*/
	public Box hasAdjacency(Position position){
		return adjacencies.get(position);
	}


	public void setColor(Color color) {
		this.color = color;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}
}
