package com.luisvdbk.game1010;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
public class MyInputProcessor extends InputAdapter {
    //DEBUG
    private final Game1010 game;
    private PiecePrinter piecePrinter;
    private BoardPrinter boardPrinter;
    private Piece touchedPiece;
    private BoxCollectionHandler collectionHandler;
    private ScoreHandler scoreHandler;
    private GameEndChecker gameEndChecker;
    private Screen actualScreen;
    private Sound placedPieceSound;

    public MyInputProcessor(final Game1010 game, final Screen actualScreen, BoardPrinter boardPrinter, PiecePrinter piecePrinter, BoxCollectionHandler collectionHandler, ScoreHandler scoreHandler, GameEndChecker gameEndChecker, Sound sound) {
        this.game = game;
        this.piecePrinter = piecePrinter;
        this.boardPrinter = boardPrinter;
        this.touchedPiece = null;
        this.collectionHandler = collectionHandler;
        this.scoreHandler = scoreHandler;
        this.gameEndChecker = gameEndChecker;
        this.actualScreen = actualScreen;
        this.placedPieceSound = sound;
    }

     public boolean touchDown (int x, int y, int pointer, int button) {
        touchedPiece = piecePrinter.touchedPiece(x, y);
        if (touchedPiece != null) {
            piecePrinter.changeToRegularSize(touchedPiece);
            piecePrinter.pieceUpOnTouch(touchedPiece, x, y);
        }
        return false;
    }

     public boolean touchUp (int x, int y, int pointer, int button) {
         if (touchedPiece != null) {
            boolean placedPiece = boardPrinter.checkPieceOnBoard(touchedPiece);
            if (placedPiece) {
                placedPieceSound.play();
                int acumPoints = collectionHandler.checkCollections() + touchedPiece.getPoints();
                scoreHandler.updatePoints(acumPoints);
                piecePrinter.removePieceFromList(touchedPiece);
                piecePrinter.renderPieces();
                if (gameEndChecker.isGameEnd()) {
                    //DEBUG
                    Gdx.app.setLogLevel(Application.LOG_DEBUG);
                    Gdx.app.debug("Game end", "No moves left");
                    actualScreen.dispose();
                    game.setScreen(new GameOverScreen(game, scoreHandler.getActualScore()));
                }
            } else {
                touchedPiece.returnToInitPos();
                piecePrinter.changeToSmallerSize(touchedPiece);
            }
            touchedPiece = null;

         }
         return false;
     }

    public boolean touchDragged (int x, int y, int pointer) {
        if (touchedPiece != null) {
            piecePrinter.pieceUpOnTouch(touchedPiece, x, y);
        }

        return false;
   }

}