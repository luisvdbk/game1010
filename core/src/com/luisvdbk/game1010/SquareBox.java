package com.luisvdbk.game1010;
import java.util.HashMap;
import java.util.Map;
public class SquareBox implements TypeOfBox {
	private int referencex;
	private int referencey;
	public SquareBox (int refx, int refy) {
		this.referencex = refx;
		this.referencey = refy;
	}
	public HashMap<Position, int[]> setAdjCoords() {

		HashMap<Position, int[]> coords = new HashMap<Position, int[]>();
		coords.put(Position.UP, new int[]{referencex - 1, referencey});
		coords.put(Position.DOWN, new int[]{referencex + 1, referencey});
		coords.put(Position.LEFT, new int[]{referencex, referencey - 1});
		coords.put(Position.RIGHT, new int[]{referencex, referencey + 1});

		return coords;
	}


}
