package com.luisvdbk.game1010;
import com.badlogic.gdx.utils.Array;

import java.util.Random;
public class PieceGenerator<E extends Enum<E> & PieceShape> {

	private int iterator = 3;
	private E[] shapes;

    public PieceGenerator(Class<E> shapeClass) {
        this.shapes = shapeClass.getEnumConstants();
    }

    public E getRandomShape() {
        Random random = new Random();
        E randomShape = shapes[random.nextInt(shapes.length)];
        return randomShape;
    }

    /*Generates a random piece using the getRandomShape() method*/
    public Array<Piece> getRandomPieces() {
    	Array<Piece> randomPieces = new Array<Piece>();
    	for (int i = 0; i < iterator; i++){
            randomPieces.add(new Piece(getRandomShape()));
    	}
    	return randomPieces;
    }

    public int getIterator(){
    	return iterator;
    }
}

