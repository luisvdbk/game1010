package com.luisvdbk.game1010;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.OrthographicCamera;


public class MainMenuScreen extends ScreenAdapter {
	private final Game1010 game;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private BitmapFont myFont;

	public MainMenuScreen(final Game1010 game){
		this.game = game;
		this.camera = game.getCamera();
		this.batch = game.getBatch();
		this.myFont = new BitmapFont(Gdx.files.internal("verdana39.fnt"),Gdx.files.internal("verdana39.png"),true);
		myFont.setColor(255, 255, 255, 1);
	}

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        float messagePosX = game.getWorldWidth()/2 - 195;
        float messagePosY = game.getWorldHeight()/2;

       	camera.update();
        batch.setProjectionMatrix(camera.combined);
		batch.begin(); 
		myFont.draw(batch, "Welcome to 1010", messagePosX, messagePosY);
		myFont.draw(batch, "Tap Anywhere to begin", messagePosX - 50, messagePosY + 50);
		batch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new MainGameScreen(game));
            dispose();
        }
    }

    @Override
    public void dispose(){
    	myFont.dispose();
    }
}