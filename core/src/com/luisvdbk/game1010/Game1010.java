package com.luisvdbk.game1010;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Game;
public class Game1010 extends Game {

	private SpriteBatch batch;
	private OrthographicCamera camera;

    private static final int VIRTUAL_WIDTH = 1080;
	private static final int VIRTUAL_HEIGHT = 1920;

    
    public void create() {
    	camera = new OrthographicCamera();
    	camera.setToOrtho(true, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);

        batch = new SpriteBatch();
		this.setScreen(new MainMenuScreen(this));
    }

    public void render() {
        super.render(); //important!
    }

    public void dispose() {
        batch.dispose();
    }

    public OrthographicCamera getCamera() {
    	return camera;
    }

    public SpriteBatch getBatch(){
    	return batch;
    }

    public int getWorldWidth(){
    	return VIRTUAL_WIDTH;
    }

    public int getWorldHeight(){
    	return VIRTUAL_HEIGHT;
    }

}