package com.luisvdbk.game1010;
import com.luisvdbk.game1010.Box;

public interface BoardShape {

	public Box[][] createBoxes();

	public void setBoxesAdjacencies();

	public Box[][] getBoxes();

	public Box getBoxById(int id);

	public Box getBoxByPosition(int x, int y);

	public int getNumberOfBoxes();


	public int[] getDimensions();

}