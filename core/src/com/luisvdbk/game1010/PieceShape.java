package com.luisvdbk.game1010;
public interface PieceShape {

	public Position[] getCoordinates();
	public Color getColor();
    public int[] getDimensions();

}