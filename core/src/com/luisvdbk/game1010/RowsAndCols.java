package com.luisvdbk.game1010;
public class RowsAndCols extends BoxCollectionHandler {

	private Board board;
	public RowsAndCols(Board board) {
		this.board = board;
		numberOfCollections = getNumberOfCollections();
		collections = new BoxCollection[numberOfCollections];
		collections = setCollections();
		
	}

	public BoxCollection[] setCollections(){
		int[] dimensions = board.getDimensions();
		Box[][] boxes = board.getBoxes();
		int rows = dimensions[0];
		int cols = dimensions[1];
		BoxCollection[] collections = new BoxCollection[numberOfCollections];

		for (int i = 0; i < rows; i++) {
			Box[] rowBoxes = new Box[cols];

			for (int j = 0; j < cols; j++) {
				//System.out.println(boxes[i][j].getId());
				rowBoxes[j] = boxes[i][j];
			}

			collections[i] = new Row(rowBoxes);
			//System.out.println("endrow");
		}

		for (int i = 0; i < cols; i++) {
			Box[] colBoxes = new Box[rows];

			for (int j = 0; j < rows; j++) {
				//System.out.println(boxes[j][i].getId());
				colBoxes[j] = boxes[j][i];
			}

			collections[rows + i] = new Column(colBoxes);
			//System.out.println("enncol");
		}
		return collections;
	}

	public int getNumberOfCollections(){
		int[] dimensions = board.getDimensions();
		int rows = dimensions[0];
		int cols = dimensions[1];
		int numOfCollections = rows + cols;

		return numOfCollections;
	}

}