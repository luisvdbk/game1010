package com.luisvdbk.game1010;
public enum Color {
	DEFAULT,
	BLUE,
	RED,
	GREEN,
	YELLOW;
}