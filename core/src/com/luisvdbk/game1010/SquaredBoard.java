package com.luisvdbk.game1010;
import java.util.HashMap;
import java.util.Set;
public class SquaredBoard implements BoardShape{
/*Applies for both square and rectangle shaped boards*/
	private Box[][] boardBoxes;
	private int rows;
	private int cols;

	public SquaredBoard(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		boardBoxes = createBoxes();
		setBoxesAdjacencies();
	}

	public Box[][] createBoxes(){
		boardBoxes = new Box[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				int id = (i * cols) + (j + 1);
				//By Default we will set squareBox as the type of Box
				boardBoxes[i][j] = new Box(id, i, j, new SquareBox(i, j));	
			}
		}
		return boardBoxes;
	}

	public void setBoxesAdjacencies() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {

				Box currentBox = boardBoxes[i][j];
				HashMap<Position, int[]> boxCoords = currentBox.getAdjCoords();
				HashMap<Position, Box> boxAdjacencies = new HashMap<Position, Box>();
				//System.out.println(currentBox.getId());
				for (HashMap.Entry<Position, int[]> entry : boxCoords.entrySet()) {
				    Position adjPos = entry.getKey();
				    int[] adjcoords = entry.getValue();
				    Box adjBox = getBoxByPosition(adjcoords[0], adjcoords[1]);
				    boxAdjacencies.put(adjPos, adjBox);
				    
					/*
				    System.out.println(adjPos);
				    if (adjBox != null) {
				    	System.out.println(adjBox.getId());
				    } else {
				    	System.out.println(adjBox);
				    }
				    */
				 

				}
				//System.out.println(boxAdjacencies);
				currentBox.setAdjacencies(boxAdjacencies);
			}
		}
	}

	public Box[][] getBoxes() {
		return boardBoxes;
	}

	public Box getBoxById(int id){
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Box box = boardBoxes[i][j];
				if (box.getId() == id) {
					return box;
				}
			}
		}

		return null;
	}

	public Box getBoxByPosition(int x, int y) {
		if ((x >= 0 && x < rows) && (y >= 0 && y < cols)) {
			Box seekedBox = boardBoxes[x][y];
			return seekedBox;
		}
		return null;
	}


	public int getNumberOfBoxes(){
		return rows * cols;
	}
	
	public int[] getDimensions(){
		int[] dimensions = new int[2];
		dimensions[0] = rows;
		dimensions[1] = cols;
		return dimensions;
	}

}